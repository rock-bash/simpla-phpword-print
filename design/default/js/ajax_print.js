$('.print-cart').live('click', function(e) {
	e.preventDefault();
	button = $(this);
	button.prop('disabled', true);
	button.val('Печатаем');

	if($('input[name="print_inner_currency"]').prop('checked'))
		print_inner_currency = 1;
	else
		print_inner_currency = 0;

	if($('input[name="print_all_prices"]').prop('checked'))
		print_all_prices = 1;
	else
		print_all_prices = 0;
	
	$.ajax({
		url: "ajax/print_products.php",
		dataType: 'json',
		method: "GET",
		data: {	print_all_prices: print_all_prices, 
				print_inner_currency: print_inner_currency},
		success: function(data){
			$('#printed_container').html(data);
			button.prop('disabled', false);
			button.val('Готово');
		}
	});
	return false;
});

$('.delete_printed').live('click', function(e) {
	e.preventDefault();
	line = $(this).parent();
	file = line.find('a').text();
	
	console.log(file);
	$.ajax({
		url: "ajax/delete_printed_product.php",
		data: {file: file},
		dataType: 'json',
		success: function(data){
			if(data)
				line.hide(100).remove();
		}
	});
	return false;
});

$(document).ready(function() {
	$('#purchases tbody').sortable({
		update: function( event, ui ) {
			document.cart.submit();
		}
	});
});
