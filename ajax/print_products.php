<?php

session_start();

chdir('..');

require_once('api/Simpla.php');

$simpla = new Simpla();

$cart = $simpla->cart->get_cart();

if(empty($cart->purchases))
	return false;



// Печатать в валюте поставки, а не в валюте магазина
$print_inner_currency = $simpla->request->get('print_inner_currency', 'boolean');

// Печатать цены ни смотря на флаг - по запросу
$print_all_prices = $simpla->request->get('print_all_prices', 'boolean');


// Получим актуальные валюты все и главну отдельно 
$currencies = $simpla->money->get_currencies(array('enabled'=>1));

if(isset($_SESSION['currency_id']))

	$currency = $simpla->money->get_currency($_SESSION['currency_id']);

else

	$currency = reset($currencies);


// Подключаем phpWord - установка по инструкции через ssh
require_once('vendor/autoload.php');

$phpWord = new \PhpOffice\PhpWord\PhpWord();

// Режим антипровал для специальных символов, если вдруг не удалось их удалить
\PhpOffice\PhpWord\Settings::setOutputEscapingEnabled(true);



// О файле
$properties = $phpWord->getDocInfo();
$properties->setCreator('Felicita63.ru');
$properties->setCompany('Felicita63.ru');
$properties->setTitle('Felicita63 - cart catalogue');
$properties->setDescription('Felicita63.ru');


$section = $phpWord->addSection();
$sectionStyle = $section->getStyle();
$sectionStyle->setMarginLeft(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(1));
$sectionStyle->setMarginRight(\PhpOffice\PhpWord\Shared\Converter::cmToTwip(.5));

$tablesStyle = array(
	'borderColor' => 'ffffff',
	'borderSize'  => 0,
	'cellMargin'  => 0,
	'marginLeft'  => 0
);

$priceStyle = array('name' => 'Helvetica', 'size' => 11, 'color' => E31E24);

$phpWord->addTableStyle('myTable', $tablesStyle);

$simple_text = array('name' => 'Helvetica', 'size' => 10);

$tables_title_width = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(19.);
$first_col_width = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(6.5);
$second_col_width = \PhpOffice\PhpWord\Shared\Converter::cmToTwip(12.94);

$phpWord->addParagraphStyle('pStyle', array('spacing' => 100, 'spaceAfter' => 200, 'indent' => 0.4));


foreach ($cart->purchases as $key=> &$p) {

	
	$tables[$key] = $section->addTable('myTable');
	
	//$section->addTitle($p->product->name, 1);
	$tables[$key]->addRow();

	// Ячейка для картинок
	$img_cell = $tables[$key]->addCell($first_col_width, array());

	if(!empty($p->product->images))
	{	
		// Берем первое изображени и добавляем его в левую колонку
		$img = array_shift($p->product->images);
		$img_cell->addImage($simpla->config->root_dir.$simpla->config->original_images_dir.$img->filename,
			array(
				'width'         => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(6.05),
				'wrappingStyle' => 'infront',
				'alignment' => 'left'
			));

		/*
		foreach ($p->product->images as $img) {
			$cell->addImage($simpla->config->root_dir.$simpla->config->original_images_dir.$img->filename,
				array(
					'width'         => \PhpOffice\PhpWord\Shared\Converter::cmToPixel(2.13),
					'wrappingStyle' => 'inline',
					'alignment' => 'left',
					'positioning'=>'relative' 
				));
		}
		*/
	}


	if($p->variant->price > 0)
	{
		if ($p->ask_for_price && ($print_all_prices != 1)){
			$img_cell->addText('Цена по запросу');
		}
		else
		{
			$currency_sign = $currency->sign;
			if(($print_inner_currency == 1) && ($p->variant->price != $p->variant->base_price))
			{
				$print_price = $p->variant->base_price;
				$print_sale_price = $p->variant->base_compare_price;
				if($p->variant->currency != $currency->id)
					$currency_sign = $currencies[$p->variant->currency]->code;
			}
			else
			{
				$print_price = $p->variant->price;
				$print_sale_price = $p->variant->compare_price;
			}

			$print_price = $simpla->money->convert($print_price);

			if($print_sale_price > 0)
			{
				$print_sale_price = $simpla->money->convert($print_sale_price);
				$img_cell->addText(	$print_sale_price, 
					array(	'name' => 'Helvetica', 
						'size' => 9, 
						'strikethrough' => true), 
					array(	'spaceBefore' => 200, 
						'spaceAfter' => 50, 
						'alignment' => 'right')
				);
				$price_paragraph_style = array('spaceBefore' => 0, 'spaceAfter' => 200, 'alignment' => 'right');
			}
			else
			{
				$price_paragraph_style = array('spaceBefore' => 200, 'spaceAfter' => 200, 'alignment' => 'right');
			}

			$img_cell->addText($print_price.' '.$currency_sign, $priceStyle, $price_paragraph_style);
		}
	}
	else
	{
		$img_cell->addText('Цена по запросу', $priceStyle, array('spaceBefore' => 200, 'spaceAfter' => 200, 'alignment' => 'right'));
	}



	// Ячейка для описаний
	$text_cell = $tables[$key]->addCell($second_col_width, array('borderLeftColor' => E31E24, 'borderLeftSize' => 8));

	// Заголовок
	$text_cell->addText($p->product->name, 
						array(	'name' => 'Helvetica', 
								'size' => 25, 
								'color' => E31E24, 
								'bold' => true, 
								'allCaps' => true ), 
						array(	'spaceAfter' => 250,
								'lineHeight' => 0.9,
								'indent' => 0.4));

	if(!empty($p->product->body))
	{	
		// Удалим подряд идущие теги
		//$p->product->body = preg_replace('/(<[^>]*>)(?:\s*\1)+/','',$p->product->body);

		$p->product->body = preg_replace('/\<br(\s*)?\/?\>/i', "\n", $p->product->body);
		$p->product->body = preg_replace('/\<li(\s*)?\/?\>/i', "\n", $p->product->body);
		$p->product->body = preg_replace('/\<tr(\s*)?\/?\>/i', "\n", $p->product->body);

		$texts = explode('</p>', $p->product->body);
		foreach ($texts as &$text) 
		{		
			$text = str_replace('&nbsp;', ' ', $text);
			$text = html_entity_decode($text, ENT_COMPAT, 'UTF-8');
			$text = strip_tags($text);

			$text_cell->addText(
				$text, $simple_text, 'pStyle'
			);
		}
	}


	// Берем все свойства товара
	$p->product->features = $simpla->features->get_product_options(array('product_id'=>$product->id));

	if(!empty($p->product->features))
	{

		// Добавляем таблицу в правой колонке
		$inner_table = $text_cell->addTable();

		foreach ($p->product->features as $feature) {

			// Проверим чтобы оно было заполнено, не создаем лишних строк
			//if(!empty($feature->value)){
				$inner_table->addRow();

				// Название свойства
				$inner_left_cell = $inner_table->addCell();
				$inner_left_cell->addText($feature->name,$simple_text); 

				// Содержание свойства
				$inner_right_cell = $inner_table->addCell();
				$inner_right_cell->addText($feature->value,$simple_text); 
			//}
		}

	}

	$section->addPageBreak();
}

//Верхний колонтитул со статическим текстом
$header = $section->createHeader();
$header->addText('Felicita63.ru', array('name' => 'Helvetica', 'size' => 22, 'color' => E31E24, 'bold'=>true));
//Нижний колонтитул с нумерацией страниц посередине
$footer = $section->createFooter();
$footer->addPreserveText('Страница {PAGE} из {NUMPAGES}', array('name' => 'Helvetica', 'bold'=>false, 'color' => E31E24), array('align'=>'center'));

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
// Тут задаем имя
$filename = 'catalogue-';
$filename .= date('j-m-y--h-i-s');
$objWriter->save($simpla->config->root_dir.'printed/'.$filename.'.docx');

//$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');
//$objWriter->save($simpla->config->root_dir.'printed/'.$filename.'.odt');


// Вывод результата - аяксом js 
$printed_files = scandir($simpla->config->root_dir.'printed/', 1);
foreach ($printed_files as $key => $pf) {
	if(($pf == '.') || ($pf == '..') || ($pf == '.htaccess'))
		unset($printed_files[$key]);
}

$simpla->design->assign('printed_files', $printed_files);

$result = $simpla->design->fetch('printed_files_list.tpl');

header("Content-type: application/json; charset=UTF-8");

header("Cache-Control: must-revalidate");

header("Pragma: no-cache");

header("Expires: -1");		

print json_encode($result);