<?php

session_start();

chdir('..');

require_once('api/Simpla.php');

$simpla = new Simpla();

$file = $simpla->request->get('file', 'string');

//$file = 'catalogue-16-10-17--04-16-20.docx';

if(file_exists($simpla->config->root_dir.'printed/'.$file))
{
	// Вывод результата - аяксом js
	/* 
	$printed_files = scandir($simpla->config->root_dir.'printed/', 1);
	foreach ($printed_files as $key => $pf) {
		if(($pf == '.') || ($pf == '..') || ($pf == '.htaccess'))
			unset($printed_files[$key]);
	}

	$simpla->design->assign('printed_files', $printed_files);

	$result = $simpla->design->fetch('printed_files_list.tpl');
	*/

	$result = unlink($simpla->config->root_dir.'printed/'.$file);
}

else

	$result = false;

header("Content-type: application/json; charset=UTF-8");

header("Cache-Control: must-revalidate");

header("Pragma: no-cache");

header("Expires: -1");		


print json_encode($result);