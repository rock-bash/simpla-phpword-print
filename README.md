# README #

Доккументация по использованию.


### Чего это вообще такое ###

Библиотека для печати каталога в word формате(odt, docx) для SimplaCMS.

Данная версия скрещена с валютным дополнением, обратите внимание.

Набрал в корзину, напечатал, подправил, отправил клиенту.

Последняя версия умеет переставлять местами товары в корзине и добавляет их по порядку.

### Что нужно установить, какие зависимости ###

Необходимо установить phpWord при помощи Composer на хосте.

https://beget.com/ru/articles/composer_install - Установка самого Composer

https://github.com/PHPOffice/PHPWord - Сам phpWord

https://webformyself.com/phpword-sozdanie-ms-word-dokumentov-sredstvami-php/ - Еще хорошая инструкция

### Непосредственная установка кода ###

Установка в ваш cart.tpl

В такой конфигурации доступен только админу.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~
{if $smarty.session.admin == 'admin'}
	<script src="design/{$settings->theme}/js/ajax_print.js?v=1.0"></script>
	<style>
	span.delete_printed {
		font-family: monospace;
		font-weight: bold;
		cursor: pointer;
	}
	.ui-sortable tr {
		cursor: n-resize;
	}
	</style>
	<br/>
	<h2>Распечатать корзину</h2>
	<input type="checkbox"  name="print_inner_currency"/> <label for="print_inner_currency">Печатать в исходной валюте</label><br/>
	<input type="checkbox"  name="print_all_prices"/> <label for="print_all_prices">Печатать все цены(вместо "По запросу" будет цена, если она не 0)</label><br/><br/>
	<input type="button" class="button print-cart" value="Распечатать"/>
	<br/>
	<br/>
	<div id="printed_container">
		{* Обновляемый аяксом список должен быть в отдельном файле *}
		{include file='printed_files_list.tpl'}
	</div>
{/if}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Предполагается, что все остальные файлы вы уже установили.

### Как со мной связаться ###

> https://vk.com/extrabash

> skype - abashyrov

> +7 937 204-69-07